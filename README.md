<h1> V1 (20180529)</h1>



<b> Method: </b>  POST

<b> URL: </b>  http://okaydocdemo.innov8tif.com/ekyc/api/ocr/v1/passport   

<h2> Request</h2>

 Mandatory Parameter:
     
<table>
<tbody>
<tr>
<td><strong>Parameter</strong></td>
<td><strong>Description</strong></td>
</tr>
<tr>
<td>
<div>
<div>apiKey</div>
</div>
</td>
<td>
<div>
<div>Request from Innov8tif (ekycsupport@innov8tif.com)</div>
</div>
</td>
</tr>
<tr>
<td>
<div>
<div>base64ImageString</div>
</div>
</td>
<td>
<div>
<div>ID Image (ID /Passport Image)</div>
</div>
</td>
</tr>
<tr>
<td>
<div>
<div>imageFormat</div>
</div>
</td>
<td>
<div>
<div>Jpeg or Png</div>
</div>
</td>
</tr>
<tr>
<td>
<div>
<div>imageEnabled</div>
</div>
</td>
<td>
<div>
<ul>
<li>True - Crop ID image will be returned.</li>
<li>False - No image will be returned.</li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>


JSON object:
<pre>
{
	"base64ImageString": "/9j/4AAQSkZJRgAB...wEwJefc\r\nf//Z\r\n",
        "apiKey":"T1XmeCOWeSfAWDUDobYbJx3i2CMEhCXF",               
        "imageFormat":".jpg",
        "imageEnabled":true
}
</pre>




<h2>Response</h2>

JSON object

Success: (If imageEnabled is set to true)
<pre>
{
    "status": "success",
    "message": "",
    "result": [{
            "ListVerifiedFields": {
                "pFieldMaps": [{
                        "wLCID": 0,
                        "FieldType": 0,
                        "wFieldType": 0,
                        "Field_MRZ": "P",
                        "Field_Visual": "P",
                        "Matrix": [
                            1,
                            0,
                            0,
                            0,
                            0,
                            3,
                            0,
                            0,
                            0,
                            0
                         ]
                        }]
                    }
    } ],
    "images":[{
               "Base64ImageString": "Base64 string",
               "Format": ".jpg",
               "LightIndex": 6,
               "PageIndex": 0
          }]
}
        
</pre>

Error:
<pre>
{
	"status": "error",
	"message": "bla bla bla"
}
</pre>

<h2> Response Result </h2>

<table>
<tbody>
<tr>
<td><b>wFieldType</b></td>
<td><b>Field</b></td>
</tr>
<tr>
<td>0</td>
<td>Document Class Code</td>
</tr>
<tr>
<td>1</td>
<td>Issuing_State_Code</td>
</tr>
<tr>
<td>2</td>
<td>Document_Number</td>
</tr>
<tr>
<td>3</td>
<td>Date_of_Expiry</td>
</tr>
<tr>
<td>4</td>
<td>Date_of_Issue</td>
</tr>
<tr>
<td>5</td>
<td>Date_of_Birth</td>
</tr>
<tr>
<td>6</td>
<td>Place_of_Birth</td>
</tr>
<tr>
<td>7</td>
<td>Personal_Number</td>
</tr>
<tr>
<td>8</td>
<td>Surname</td>
</tr>
<tr>
<td>9</td>
<td>Given_Names</td>
</tr>
<tr>
<td>10</td>
<td>MOther Names</td>
</tr>
<tr>
<td>11</td>
<td>Nationality</td>
</tr>
<tr>
<td>12</td>
<td>Sex</td>
</tr>
<tr>
<td>13</td>
<td>Height</td>
</tr>
<tr>
<td>14</td>
<td>Weight</td>
</tr>
<tr>
<td>17</td>
<td>Address</td>
</tr>
<tr>
<td>24</td>
<td>Authority</td>
</tr>
<tr>
<td>25</td>
<td>Surname_And_Given_Names</td>
</tr>
<tr>
<td>26</td>
<td>Nationality_Code</td>
</tr>
<tr>
<td>36</td>
<td>MRZ Name</td>
</tr>
<tr>
<td>38</td>
<td>Issuing State Name</td>
</tr>
<tr>
<td>39</td>
<td>Place of Issue</td>
</tr>
<tr>
<td>51</td>
<td>MRZ Strings</td>
</tr>
<tr>
<td>80</td>
<td>Document Number CheckDigit</td>
</tr>
<tr>
<td>81</td>
<td>Date of Birth CheckDigit</td>
</tr>
<tr>
<td>82</td>
<td>Date of Expiry CheckDigit</td>
</tr>
<tr>
<td>84</td>
<td>FinalCheckDigit</td>
</tr>
<tr>
<td>172</td>
<td>MRZ Strings With Correct CheckSums</td>
</tr>
<tr>
<td>185</td>
<td>Age</td>
</tr>
<tr>
<td>195</td>
<td>Optional Data CheckDigit</td>
</tr>
<tr>
<td>363</td>
<td>Religion</td>
</tr>
<tr>
<td>364</td>
<td>Remainder Term</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>

